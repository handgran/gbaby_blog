<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Gebaby
 */

get_header(); ?>

	<!-- PÁGINA INICIAL -->
	<div class="pg pg-inicial">
		
		<!-- CARROSSEL DE DESTAQUE -->
		<section class="carrosselDestaque">
			<h6 class="hidden">Carrossel de destaques</h6>
			<div id="carrosselDestaque" class="owl-carousel">
				<?php 
					//LOOP DE POST DESTAQUES
					$postDestaques = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
					while ( $postDestaques->have_posts() ) : $postDestaques->the_post();

						$fotoDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoDestaque = $fotoDestaque[0];
				?>
				<div class="item">
					<figure>
						<a href="">
							<img src="<?php echo  $fotoDestaque ?>" alt="<?php echo get_the_title() ?>">
						</a>
					</figure>
				</div>
				<?php endwhile; wp_reset_query(); ?>
			</div>
		</section>

		<div class="conteudo">
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
						<section class="posts">
							<h6 class="hidden">Posts do Blog</h6>

							<ul>
								<?php 
									$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
									$fotoPost = $fotoPost[0];
										
								?>
								<li id="post">
								
									<div class="catData">
										<h2 class="cat">Bebê</h2> 
										<span><?php the_time('j') ?>.<?php the_time('m') ?>.<?php the_time(' Y') ?></span>
									</div>
									<h1 class="titulo"><?php echo get_the_title() ?></h1>
									<img src="<?php bloginfo('template_directory'); ?>/img/detalhePost.png" class="detalhe" alt="Detalhe">
									<figure>
										<img src="<?php echo $fotoPost ?>" alt="<?php echo get_the_title() ?>">
									</figure>
									<div class="conteudoSingle">
											<?php echo the_content()?>

											<div class="compartilhar">
												<a  href="http://facebook.com/share.php?u=<?php the_permalink() ?>&amp;t=<?php echo urlencode(the_title('','', false)) ?>" target="_blank" title="Compartilhar <?php the_title();?> no Facebook">Compartilhar</a> 
											</div>
									</div>
									<div class="disqus">	
									<?php 	
										// If comments are open or we have at least one comment, load up the comment template.
										if ( comments_open() || get_comments_number() ) :
											comments_template();
										endif; 
									?>
									</div>
								</li>
								<?php wp_reset_query(); ?>
							</ul>				
							
							
						</section>
						
						<div class="carrosselOutros"  id="carrosselPost" class="owl-carousel">
							
							<?php 
									$postEventos = new WP_Query(array(
										'post_type'     => 'post',
										'posts_per_page'   => -1,
										'tax_query'     => array(
											array(
												'taxonomy' => 'category',
												'field'    => 'slug',
												'terms'    => 'indicados',
												)
											)
										)
									);
									while ( $postEventos->have_posts() ) : $postEventos->the_post();
										$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$fotoPost = $fotoPost[0];
						 	
							?>
							<div class="item" >
								<a href="<?php echo get_permalink() ?>" title="<?php echo get_the_title() ?>">
									<figure>
										<img src="<?php echo $fotoPost ?>" alt="<?php echo get_the_title() ?>">
									</figure>
									<p><?php echo get_the_title() ?></p>
								</a>
							</div>
							<?php endwhile; wp_reset_query(); ?>
						</div>
							
						<div class="paginador">
							<div class="row">
								<div class="col-sm-6">
									<div class="linksLeft">
										<?php next_post('%','	Postagem mais recentes ', 'no') ?>
									</div>
								</div>
								<div class="col-sm-6 text-right">
									<div class="linksRihgt">
										
										<?php previous_post('%',' Postagem mais antiga ', 'no') ?>
									</div>
								</div>
							</div>
						</div> 

					</div>
					
					<?php echo get_sidebar(); ?>
					
				</div>
			</div>
		</div>

	</div>

<?php
get_sidebar();
get_footer();
