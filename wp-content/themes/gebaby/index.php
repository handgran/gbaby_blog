<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Gebaby
 */

get_header(); ?>

	<!-- PÁGINA INICIAL -->
	<div class="pg pg-inicial">
		
		<!-- CARROSSEL DE DESTAQUE -->
		<section class="carrosselDestaque">
			<h6 class="hidden">Carrossel de destaques</h6>
			<div id="carrosselDestaque" class="owl-carousel">
				<?php 
					//LOOP DE POST DESTAQUES
					$postDestaques = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
					while ( $postDestaques->have_posts() ) : $postDestaques->the_post();

						$fotoDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoDestaque = $fotoDestaque[0];
				?>
				<div class="item">
					<figure>
						<a href="<?php echo $subtitulo_post = rwmb_meta('Gebaby_destaque_link'); ?>" target="_blank" >
							<img src="<?php echo  $fotoDestaque ?>" alt="<?php echo get_the_title() ?>">
						</a>
					</figure>
				</div>
				<?php endwhile; wp_reset_query(); ?>
			</div>
		</section>

		<div class="conteudo">
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
						<section class="posts">
							<h6 class="hidden">Posts do Blog</h6>

							<ul id="posts">
								<?php 
									//* Start the Loop */
									while ( have_posts() ) : the_post();

										$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$fotoPost = $fotoPost[0];
										$category = get_the_category(); 
										
								?>

								<li>
									<a href="<?php echo get_permalink() ?>/#post ">
										<div class="catData">
											<h3><?php echo $category[0]->cat_name; ?></h3> 
											<span><?php the_time('j') ?>.<?php the_time('m') ?>.<?php the_time(' Y') ?></span>
										</div>
										<h2><?php echo get_the_title() ?></h2>
										<img src="<?php bloginfo('template_directory'); ?>/img/detalhePost.png" class="detalhe" alt="Detalhe">
										<figure>
											<img src="<?php echo $fotoPost ?>" alt="<?php echo get_the_title() ?>">
										</figure>
										<p><?php echo customExcerpt(300); ?></p>
										<span class="lerMais">Continue Lendo <small></small></span>
									</a>
								</li>
								<?php endwhile; wp_reset_query(); ?>
							</ul>				
							

						</section>
						<div class="paginador">
							<ul class="text-center">
								<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
							</ul>
						</div>
					</div>
					
					<?php echo get_sidebar(); ?>
					
				</div>
			</div>
		</div>

	</div>

<?php

get_footer();
