<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Gebaby
 */

get_header(); ?>

	<!-- PÁGINA INICIAL -->
	<div class="pg pg-inicial">
		
		<!-- CARROSSEL DE DESTAQUE -->
		<section class="carrosselDestaque">
			<h6 class="hidden">Carrossel de destaques</h6>
			<div id="carrosselDestaque" class="owl-carousel">
				<?php 
					//LOOP DE POST DESTAQUES
					$postDestaques = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
					while ( $postDestaques->have_posts() ) : $postDestaques->the_post();

						$fotoDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoDestaque = $fotoDestaque[0];
				?>
				<div class="item">
					<figure>
						<a href="<?php echo $subtitulo_post = rwmb_meta('Gebaby_destaque_link'); ?>" target="_blank" >
							<img src="<?php echo  $fotoDestaque ?>" alt="<?php echo get_the_title() ?>">
						</a>
					</figure>
				</div>
				<?php endwhile; wp_reset_query(); ?>
			</div>
		</section>
		<div class="container">
			<h1 class="page-title" style="padding-top: 20px;
    display: block;
    text-align: center;
    color: #868bc4;
    background: #fff;
    padding-bottom: 20px;"><?php
			/* translators: %s: search query. */
			printf( esc_html__( 'Você pesquisou por: %s', 'gebaby' ), '<span>' . get_search_query() . '</span>' );
		?></h1>
		</div>
		<div class="conteudo">
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
						<section class="posts">

							<h6 class="hidden">Posts do Blog</h6>

							<ul>
								<?php 
								//* Start the Loop */
								while ( have_posts() ) : the_post();
									$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );

										$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$fotoPost = $fotoPost[0];
										$category = get_the_category(); 
							
										
								?>

								<li>
									<a href="<?php echo get_permalink() ?> ">
										<div class="catData">
											<h3><?php echo $category[0]->cat_name; ?></h3> 
											<span><?php the_time('j') ?>.<?php the_time('m') ?>.<?php the_time(' Y') ?></span>
										</div>
										<h2><?php echo get_the_title() ?></h2>
										<img src="<?php bloginfo('template_directory'); ?>/img/detalhePost.png" class="detalhe" alt="Detalhe">
										<figure>
											<img src="<?php echo $fotoPost ?>" alt="<?php echo get_the_title() ?>">
										</figure>
										<p><?php echo customExcerpt(300); ?></p>
										<span class="lerMais">Continue Lendo <small></small></span>
									</a>
								</li>
								<?php endwhile; wp_reset_query(); ?>
							</ul>				

						</section>
						<!-- <div class="paginador">
							<a href="">Postagem mais recentes</a>
							<a href="">Postagem mais antiga</a>
						</div> -->
					</div>
					
					<?php echo get_sidebar(); ?>
					
				</div>
			</div>
		</div>

	</div>

<?php
get_sidebar();

