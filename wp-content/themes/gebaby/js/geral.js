
$(function(){


	/*****************************************
		SCRIPTS PÁGINA  POST
	*******************************************/
	$(document).ready(function() {
			

	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	$("#carrosselDestaque").owlCarousel({
			items : 1,
			dots: true,
			loop: false,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,
			margin: 25,
			center:true,
			animateOut: 'fadeOut',
		});

		// SCRIPT CARROSSEL DE POSTS
		$("#carrosselPost").owlCarousel({
			items : 4,
			dots: true,
			loop: false,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,
			margin: 5,
			animateOut: 'fadeOut',
			responsiveClass:true,			    
	        responsive:{
	            320:{
	                items:1
	            },
	            600:{
	                items:2
	            },
	           
	            991:{
	                items:3
	            },
	            1024:{
	                items:4
	            },
	            1440:{
	                items:4
	            },
	            			            
	        }	
		});

		// BOTÕES DO CARROSSEL
		var carrossel_post = $("#carrosselPostSimilares").data('owlCarousel');
		$('#btnCarrosselPostSimilaresLeft').click(function(){ carrossel_post.prev(); });
		$('#btCcarrosselPostSimilaresRight').click(function(){ carrossel_post.next(); });

		
		
	});

	$(document).ready(function(){
		 
	    var speed = 1000;

	    // check for hash and if div exist... scroll to div
	    var hash = window.location.hash;
	    if($(hash).length) scrollToID(hash, speed);

	    // scroll to div on nav click
	    $('a').click(function (e) {
	        e.preventDefault();
	        var id = $(this).data('id');
	        var href = $(this).attr('href');
	        if(href === '#'){
	            scrollToID(id, speed);
	        }else{
	            window.location.href = href;
	        }

	    });

    	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
    	      var target = $(this.hash);
    	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
    	      if (target.length) {
    	        $('html,body').animate({
    	          scrollTop: target.offset().top
    	        }, 1000);
    	        return false;
    	      }
    	    }
	})

	function scrollToID(id, speed) {
	    var offSet = 70;
	    var obj = $(id).offset();
	    var targetOffset = obj.top - offSet;
	    $('html,body').animate({ scrollTop: targetOffset }, speed);
	}
	
		$.ajax({
		type: "GET",
		dataType: "jsonp",
		cache: false,
		url: "https://api.instagram.com/v1/media/{media-id}?access_token=d3e95c5bec6041b596be8069a0613adb",
		success: function(data) {
			      console.log(data);
			    }
			});
	
});
