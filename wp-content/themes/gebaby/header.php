<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Gebaby
 */
global $configuracao;
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	
	<!-- META -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta property="og:title" content="" />
	<meta property="og:description" content="" />
	<meta property="og:url" content="" />
	<meta property="og:image" content=""/>
	<meta property="og:type" content="website" />
	<meta property="og:site_name" content="" />


	<!-- CSS -->
	<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
	
	<!-- FAVICON -->
  <link rel="shortcut icon" href="https://cdn.awsli.com.br/28/28169/favicon/8bc5f03d73.png" />
  <link rel="icon" href="https://cdn.awsli.com.br/28/28169/favicon/8bc5f03d73.png" sizes="192x192">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<!-- TOPO -->
	<header class="topo">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="logo">
						<a href="<?php echo home_url( '/' ); ?>">
							<img src="<?php bloginfo('template_directory'); ?>/img/logoMArca.png" alt="<?php echo get_bloginfo()  ?> ">
						</a>
					</div>
				</div>
				<div class="col-sm-9">
					<div class="navbar" role="navigation">	

						<!-- MENU MOBILE TRIGGER -->
						<button type="button" id="botao-menu" class="navbar-toggle collapsed hvr-pop" data-toggle="collapse" data-target="#collapse">
							<span class="sr-only"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						<!--  MENU MOBILE-->
						<div class="row navbar-header">			
							<nav class="collapse navbar-collapse" id="collapse">
								<ul class="nav navbar-nav">			
									<?php
										// LISTA DE CATEGORIAS
										$arrayCategorias = array();
										$categorias=get_categories($args);
										$i = 0;
										foreach($categorias as $categoria):
											$arrayCategorias[$categoria->cat_ID] = $categoria->name;
											$nomeCategoria = $arrayCategorias[$categoria->cat_ID];
											if ($i<5):
												if ($nomeCategoria != "Indicados" && $nomeCategoria != "Sem categoria") {
												
											
									?>
									<li><a href="<?php echo get_category_link($categoria->cat_ID); ?>" class="" title="<?php echo $nomeCategoria; ?>"><?php echo $nomeCategoria; ?></a></li>
									<?php 	}endif;$i++;endforeach; ?>
									<li><a href="https://www.gebaby.com.br/" class="" title="Loja Virtual">Loja Virtual</a></li>
								</ul>
							</nav>						
						</div>			
					</div>
				</div>
			</div>
		</div>
	</header>
