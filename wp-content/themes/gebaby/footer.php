<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Gebaby
 */

?>

	<!-- RODAPÉ -->
		<footer class="rodape">
			<div class="container">
				<div class="row">
					<div class="col-sm-1">
						<div class="logoRodape">
							<img src="<?php bloginfo('template_directory'); ?>/img/logoRodape.png" alt="">
						</div>
					</div>
					<div class="col-sm-3">
						<div class="menu">
							<strong>Contato</strong>
							<a href="https://www.gebaby.com.br/" target="blank" class="linkSite"><i class="fas fa-heart"></i> gebaby.com.br</a>
							<a href="tel:(41)995667314" class="" target="blank" class="numero"><i class="fas fa-mobile-alt"></i> (41)9 9566-73-14</a>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="menu">
							<strong>Redes Sociais</strong>
							<a href="https://www.facebook.com/gebabyonline" target="blank" class="face"><i class="fab fa-facebook"></i> facebook/gebabyonline</a>
							<a href="https://www.instagram.com/explore/tags/gebaby/" target="blank" class="gebay"><i class="fab fa-instagram"></i> @gebabyonline</a>
							<a href="https://br.pinterest.com/gebabyonline/" target="blank" class="pinterest"><i class="fab fa-pinterest-p"></i> pinterest.com/gebabyonline/</a>
						</div>
					</div>
					<div class="col-sm-5">
						<div class="menu">
							<strong>Fale com a Gente! <i class="fas fa-heart"></i> </strong>
						</div>
						<div class="formFooter">
							<!-- <div class="input">
								<small  style="background: url(<?php bloginfo('template_directory'); ?>/img/iconeRodape1.png);"></small>
								<input type="text" placeholder="Nome">
							</div>

							<div class="input">
								<small style="background: url(<?php bloginfo('template_directory'); ?>/img/iconeradpe2.png);"></small>
								<input type="text" placeholder="E-mail">
							</div>

							<div class="input">
								<small  style="background: url(<?php bloginfo('template_directory'); ?>/img/iconeRodape3.png);"></small>
								<textarea placeholder="Mensagem"></textarea>
							</div>

							<input type="submit" value="Enviar">
 -->						
						 <?php echo do_shortcode('[contact-form-7 id="25" title="Formulário de contato"]'); ?>
						</div>
					</div>
				</div>
			</div>

			<div class="copyright">
				<p>TODOS OS DIREITOS RESERVADOS GEBABY © DESIGN BY <a href="http://handgran.com" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/img/logohand.png" alt="Handgran"></a></p>
			</div>
		</footer>

<?php wp_footer(); ?>

</body>
</html>
