<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Gebaby
 */
global $configuracao;
if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>
<div class="col-sm-4">
	<aside>
		
		<div class="areaSobre">
			<article>
				<figure>
					<img src="<?php echo $configuracao['logo_sidebar']['url'] ?>" alt="<?php echo get_bloginfo()  ?>">
				</figure>
				<p><?php echo $configuracao['text_sidebar'] ?></p>
			</article>
		</div>

		<div class="areaLinksexternos">
			<a href="https://www.facebook.com/gebabyonline" target="_blank"><i class="fab fa-facebook-f"></i></a>
			<a href="https://www.instagram.com/explore/tags/gebaby/" target="_blank"><i class="fab fa-instagram"></i></a>
			<a href="https://br.pinterest.com/gebabyonline/" target="_blank"><i class="fab fa-pinterest-square"></i></a>
			<a href="https://www.gebaby.com.br/" target="_blank" class="linkSite"></a>
		</div>

		<div class="areaPesquisa">
			<div class="form">
				<form role="search" method="get" action="<?php echo home_url( '/' ); ?>">
					<input type="text" name="s" id="campoPesquisa" placeholder="Pesquisar">
					<input type="submit" value="&#xf002">
				</form>
			</div>
		</div>

		<div class="areaFacebook">
			<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fgebabycentro%2F&tabs=timeline&width=340&height=256&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1544579195844789" height="256" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
		</div>

		<div class="areaIntagran areaSide">
			<?php echo do_shortcode('[instagram-feed]'); ?>
			<!-- <h2>@Gebabyonline</h2> -->
			<a href="https://www.instagram.com/explore/tags/gebaby/" target="_blank">
				<img src="<?php bloginfo('template_directory'); ?>/img/insta.png" alt="">
			</a>

		</div>

		<div class="pluginParaSite">
			
		</div>

		<?php 

			$post_tags = get_the_tags();  
			if ( $post_tags ):
		 ?>
		<div class="tagsBlog areaSide">
			<h2>Tags</h2>
			<?php foreach( $post_tags as $tag ): ?>
			<a href="<?php bloginfo('url');?>/tag/<?php echo $tag->slug; ?>"><?php echo $tag->name; ?></a>
		<?php endforeach; ?>
		
		</div>
	<?php  endif; ?>
	</aside>
</div>